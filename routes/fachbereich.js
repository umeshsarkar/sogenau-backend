const express = require('express');
const router = express.Router();
const axios = require('axios');
require("dotenv").config();

const endpoint = `${process.env.ENDPOINT}`
const authKey = `${process.env.AUTH_KEY}`;
const headers = {
    'Content-Type': 'application/json',
    'Authorization': `Bearer ${authKey}`
};

// GET /contacts
router.get('/', async (req, res) => {
    try {
        const response = await axios.get(endpoint, { headers });
        res.send(response.data);
    } catch (error) {
        console.error(error);
        res.status(500).send('Error getting contacts');
    }
});

module.exports = router;
