const express = require('express');
const cors = require('cors');
const app = express();
const bodyParser = require('body-parser');
const contactsRouter = require('./routes/fachbereich.js');

const port = process.env.PORT || 5000

// Allow requests from localhost:4200
const corsOptions = {
    origin: 'http://localhost:4200',
    optionsSuccessStatus: 200 // some legacy browsers (IE11, various SmartTVs) choke on 204
};

app.use(cors(corsOptions));
app.use(bodyParser.json());
app.use('/fachbereich', contactsRouter);

app.listen(port, () => {
    console.log(`Server listening at http://localhost:${port}`);
});
